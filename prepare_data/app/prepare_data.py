MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/housing_test_2/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    housing = pickle.load(open(f"{PICKLE_PATH}/housing.pkl", "rb"))
    train_set = pickle.load(open(f"{PICKLE_PATH}/train_set.pkl", "rb"))
    test_set = pickle.load(open(f"{PICKLE_PATH}/test_set.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xpr_param_component_name = prepare_data	
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH","housing","train_set", "test_set"]

train_x = train_set.drop("median_house_value", axis=1) # drop labels for training set
housing_labels = train_set["median_house_value"].copy()
imputer = SimpleImputer(strategy="median")
#housing_num = train_x.drop("ocean_proximity", axis=1)
# alternatively: 
housing_num = train_x.select_dtypes(include=[np.number])
imputer.fit(housing_num)
housing_cat = housing[["ocean_proximity"]]

ordinal_encoder = OrdinalEncoder()
housing_cat_encoded = ordinal_encoder.fit_transform(housing_cat)


#ordinal_encoder.categories_

cat_encoder = OneHotEncoder()
housing_cat_1hot = cat_encoder.fit_transform(housing_cat)

#housing_cat_1hot.toarray()
cat_encoder = OneHotEncoder(sparse=False)
housing_cat_1hot = cat_encoder.fit_transform(housing_cat)


#cat_encoder.categories_

# # column index
# rooms_ix, bedrooms_ix, population_ix, households_ix = 3, 4, 5, 6

# class CombinedAttributesAdder(BaseEstimator, TransformerMixin):
#     def __init__(self, add_bedrooms_per_room = True): # no *args or **kargs
#         self.add_bedrooms_per_room = add_bedrooms_per_room
#     def fit(self, X, y=None):
#         return self  # nothing else to do
#     def transform(self, X):
#         rooms_per_household = X[:, rooms_ix] / X[:, households_ix]
#         population_per_household = X[:, population_ix] / X[:, households_ix]
#         if self.add_bedrooms_per_room:
#             bedrooms_per_room = X[:, bedrooms_ix] / X[:, rooms_ix]
#             return np.c_[X, rooms_per_household, population_per_household,
#                          bedrooms_per_room]
#         else:
#             return np.c_[X, rooms_per_household, population_per_household]

# attr_adder = CombinedAttributesAdder(add_bedrooms_per_room=False)
# housing_extra_attribs = attr_adder.transform(housing.values)

imputer = SimpleImputer(strategy="median")

num_pipeline = Pipeline([
        ('imputer', SimpleImputer(strategy="median")),
        ('std_scaler', StandardScaler()),
    ])

housing_num_tr = num_pipeline.fit_transform(housing_num)
#housing_num_tr

num_attribs = list(housing_num)
cat_attribs = ["ocean_proximity"]

full_pipeline = ColumnTransformer([
        ("num", num_pipeline, num_attribs),
        ("cat", OneHotEncoder(), cat_attribs),
    ])

housing_prepared = full_pipeline.fit_transform(train_x)
housing_prepared
housing_prepared.shape


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/housing_test_2/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(housing, open(f"{PICKLE_PATH}/housing.pkl", "wb"))
    pickle.dump(train_set, open(f"{PICKLE_PATH}/train_set.pkl", "wb"))
    pickle.dump(test_set, open(f"{PICKLE_PATH}/test_set.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
