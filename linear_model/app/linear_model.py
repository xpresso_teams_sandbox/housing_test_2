MOUNT_PATH = None
import os
import sys
import types
import pickle
import urllib
import marshal
import sklearn
import tarfile
import warnings
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from pandas.plotting import scatter_matrix
from sklearn.compose import ColumnTransformer
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import mean_absolute_error
from sklearn.preprocessing import OrdinalEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedShuffleSplit
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/housing_test_2/pickles"

try:
    MOUNT_PATH = pickle.load(open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "rb"))
    housing_labels = pickle.load(open(f"{PICKLE_PATH}/housing_labels.pkl", "rb"))
    housing_prepared = pickle.load(open(f"{PICKLE_PATH}/housing_prepared.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print("failed to load pickle, ignored")
    print(str(e))

## $xpr_param_component_name = linear_model	
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["MOUNT_PATH","housing_labels","housing_prepared"]






#Fit Model : Linear Regression 


lin_reg = LinearRegression()
lin_reg.fit(housing_prepared, housing_labels)

housing_predictions = lin_reg.predict(housing_prepared)
lin_mse = mean_squared_error(housing_labels, housing_predictions)
lin_rmse = np.sqrt(lin_mse)
lin_rmse

lin_mae = mean_absolute_error(housing_labels, housing_predictions)
lin_mae


PICKLE_PATH = "/mnt/nfs/data/jupyter_experiments/projects/housing_test_2/pickles"

try:
    pickle.dump(MOUNT_PATH, open(f"{PICKLE_PATH}/MOUNT_PATH.pkl", "wb"))
    pickle.dump(housing_labels, open(f"{PICKLE_PATH}/housing_labels.pkl", "wb"))
    pickle.dump(housing_prepared, open(f"{PICKLE_PATH}/housing_prepared.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
